# This repository contains my test projects for learning k8s

## Some useful kubectl commands
 - Attach to a pod  
   kubectl exec <pod_id> /bin/sh -n <namespace> -it
 - Port mapping to a pod  
   kubectl -n <namespace> port-forward pod/<pod_id> 8080:8080
 - Connect to pod with debug tools  
   kubectl-debug <pod_id> --agentless=true --port-forward=true --agent-image=kiyanser/debug-agent:v0.1 -n <namespace>
 - Check logs of node
   journalctl -u kubelet | grep <pod_id>

## Some useful helm commands
 - Inititate starter chart  
   helm create <chart_name>
 - Release helm chart  
   helm install <release_name> <chart_name> -n <namespace> -f <chart_name/values.yaml>
 - Delere helm release  
   helm uninstall <release_name> -n namespace

## Some useful minikube commands
 - Pull image to minikube from registry  
   minikube image pull <image_name:tag>
 - Load image to minikube from local docker
   minikube image load <image_name:tag>
 - Assign external IP to a service  
   minikube service <service_name> -n <namespace>
 - Enable ingress  
   minikube addons enable ingress
 - Start minikube with calico container network interface(CNI)  
   minikube start --cni calico 

## Some useful commands
 - Generate self-signed ssl certificate  
   openssl req -x509 -nodes -days <days_count> -newkey rsa:2048 -out <filename>.crt -keyout <filename>.key -subj "/CN=<site name>/O=<filename>"