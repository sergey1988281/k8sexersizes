#!/bin/bash

read -p "
This init script will pre-configure your local system.
Would you like to continue?(yes/no)" yn

case $yn in 
    yes ) echo ok, we will proceed;;
    no ) echo exiting...;
        exit;;
    * ) echo invalid response;
        exit 1;;
esac

echo "Creating /mnt/data for persistent volume mounting"
mkdir -p /mnt/data
echo "Creating persistent volume"
kubectl delete -f persistentVolume.yaml && kubectl apply -f persistentVolume.yaml